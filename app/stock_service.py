import os
import json
import requests

from flask import (Flask, Response)
#from flask.ext.api import status

app = Flask(__name__)

# Lookup STOCK_SYMBOL environment variable
try:
    stock_symbol   = os.environ['STOCK_SYMBOL']
except KeyError:
    print('OS Environment variable "STOCK_SYMBOL" is not defined.')
    exit(1)

# Lookup NUM_DAYS environment variable
try:
    number_of_days = int(os.environ['NUM_DAYS'])
except KeyError:
    print('OS Environment variable "NUM_DAYS" is not defined.')
    exit(1)

# Lookup API_KEY environment variable
try:
    alphavantage_api_key = os.environ['API_KEY']
except KeyError:
    print('OS Environment variable "API_KEY" is not defined.')
    exit(1)

# compact output size <= 100 data points
API_URL = "{scheme}://www.alphavantage.co/query?function={time_series}&symbol={symbol}&outputsize={output_size}&apikey={api_key}".format(
    scheme='https',
    time_series='TIME_SERIES_DAILY_ADJUSTED',
    symbol=stock_symbol,
    api_key=alphavantage_api_key,
    output_size='compact',
)


@app.route("/", methods=['GET'])
def hello():
    '''
     Endpoint for help message
    '''
    return Response('<p>Valid endpoints: /health, /get-stock-data</p>', status=200, mimetype='text/html')
    

@app.route("/health", methods=['GET'])
def health():
    '''
      Endpoint for health checks
    '''
    return Response('OK', status=200, mimetype='text/plain')


@app.route("/get-stock-data", methods=['GET'])
def get_stock_data():
    '''
      Endpoint for stock data
       Stock symbol and number of days data to return are
       based on os environment variables.

        STOCK_SYMBOL 
        NUM_DAYS 
    '''
    try:
        r = requests.get(API_URL)
    except:
        pass

    if r.status_code == 200:
        try:
            content = (json.loads(r.text))['Time Series (Daily)']

            # Get the dates from sorted data, with the latest data first
            dates = []
            for x in list(reversed(sorted(content)))[0:number_of_days]:
                dates.append(x)

            # populate dictionary with the data matching the date range
            data = dict((k,v) for k,v in content.items() if k in dates)

            # Calculate sum of closing price
            sum_of_close = 0
            for k,v in data.items():
                sum_of_close += float(v['4. close'])

            data['average_close'] = sum_of_close / number_of_days

            data['stock'] = stock_symbol

            response_code = '200'
            
        except:
            pass

    else:
        data = '{ "Error": "Lookup failed" }'
        response_code = '500'

    return Response(json.dumps(data), status=response_code, mimetype='application/json')



### Start up settings
if __name__ == "__main__":
    app.run(host="0.0.0.0")
