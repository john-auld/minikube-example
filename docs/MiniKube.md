# Instructions for running the stock service on Kubernetes

The procedure requires a vanilla kubernetes environment. The steps here were tested on a Mac OSX release of minkkube.


## Check the status of minikube.

If needed, start minikube.
```
minikube start
```

Check the status of minikube
```
minikube status
host: Running
kubelet: Running
apiserver: Running
kubectl: Correctly Configured: pointing to minikube-vm at 192.168.99.100
```

Populate kubernetes secrets
```
kubectl create -f k8s/secrets.yaml 
secret/stock-service created
```

Check the secrets
```
kubectl describe secrets/stock-service
Name:         stock-service
Namespace:    default
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
apikey:  12 bytes
```




Apply the manifest
```
kubectl apply -f k8s/deployment.yaml
  deployment.apps/stock-service created
```

Apply the service
```
kubectl apply -f k8s/service.yaml 
service/stock-service created
```

Expose an ingress port
```
kubectl apply -f k8s/node_port.yaml 
service/np-stock-service created
```

Check the status of the deployment and service
```
kubectl get all
NAME                                 READY   STATUS    RESTARTS   AGE
pod/stock-service-5897cf9bf8-5bxq8   1/1     Running   0          12m


NAME                       TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
service/kubernetes         ClusterIP   10.96.0.1       <none>        443/TCP          47h
service/np-stock-service   NodePort    10.102.36.221   <none>        8000:30000/TCP   26s
service/stock-service      ClusterIP   10.108.37.171   <none>        8000/TCP         2m10s


NAME                            READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/stock-service   1/1     1            1           12m

NAME                                       DESIRED   CURRENT   READY   AGE
replicaset.apps/stock-service-5897cf9bf8   1         1         1       12m
```

Check the container logs
```
kubectl logs deployment.apps/stock-service
[2019-08-30 12:22:07 +0000] [1] [INFO] Starting gunicorn 19.9.0
[2019-08-30 12:22:07 +0000] [1] [INFO] Listening at: http://0.0.0.0:8000 (1)
[2019-08-30 12:22:07 +0000] [1] [INFO] Using worker: sync
[2019-08-30 12:22:07 +0000] [14] [INFO] Booting worker with pid: 14
```

Get the URL for the stock-service
```
minikube service np-stock-service --url
http://192.168.99.100:30000
```

Test the health service endpoint
```
curl -s http://192.168.99.100:30000/health && echo
OK
```





