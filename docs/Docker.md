# Build notes for the docker image.

Repository: https://gitlab.com/john-auld/minikube-example

## Docker image 

These steps have been tested on a CentOS 7 having a working docker installation.

- checkout the master branch of the repository.
- run the commands below

### Build image
```
docker login registry.gitlab.com

# The tag, 1.0, is an example. Use an appropriate value.
docker build -t registry.gitlab.com/john-auld/minikube-example:1.0 .

sudo docker images
  registry.gitlab.com/john-auld/minikube-example   1.0                 c13a8b2c7343        About a minute ago   932 MB
```


### Push image to the docker registry
The registry login step is included above in the build section.


```
docker push registry.gitlab.com/john-auld/minikube-example:1.0

In the Gitlab web UI, check that the image has been added to the registry.

https://gitlab.com/john-auld/minikube-example/container_registry
```

### Remvove test container and image on build node
These steps are optional, but they can be followed to remove temporary artifacts from the build node.

```
sudo docker ps -a | awk '$0 ~ /minikube-example/ {print $1}' | xargs -I % sudo docker rm -f %

sudo docker images | awk '$0 ~ /minikube-example/ {print $3}' | xargs -I % sudo docker image rm %
```
