FROM python:3.7
ADD ./app /app
WORKDIR /app
RUN pip install flask gunicorn requests
EXPOSE 8000
ENV STOCK_SYMBOL "MSFT"
ENV NUM_DAYS "5"
ENV API_KEY "demo"
CMD ["gunicorn", "-b", "0.0.0.0:8000", "-w", "1", "stock_service:app"]
