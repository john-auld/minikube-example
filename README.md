# Minikube Demonstration project

This project demonstrates a simple Python Flask service being hosted by kubernetes.

## Requirements

- A vanilla instance of minikube
- A working instance of docker

## Docker image build notes

An image is available in the repo with public access, so it should not be necessary for the reader to run the steps below.

*TL/DR*

```
git clone https://gitlab.com/john-auld/minikube-example
sudo su
docker login registry.gitlab.com
docker build -t registry.gitlab.com/john-auld/minikube-example:1.0 .
docker push registry.gitlab.com/john-auld/minikube-example:1.0
exit
```

Full documentation is given in docs/Docker.md.


## Minikube instructions

*TL/DR*

```
minikube start
kubectl create -f k8s/secrets.yaml
kubectl apply -f k8s/deployment.yaml
kubectl apply -f k8s/service.yaml
kubectl apply -f k8s/node_port.yaml

minikube service np-stock-service --url
  http://192.168.99.100:30000

curl -s http://192.168.99.100:30000/health && echo
  OK
```

Full documentation is given in docs/MiniKube.md


## Endpoint details

The following endpoints are available on the stock data service.

- "/"
  Returns help information.
- "/health"
   Returns "OK" and the HTTP status code 200, which is useful for load balancer health checks.
- "/get-stock-data"
  Returns a json object with details of the stock ticker symbol given in the environment variable "STOCK_SYMBOL" for the number of days given by the environment variable "NUM_DAYS". These variables are configured in the file k8s/deployment.yaml. The backend service also requires an API key, which is defined in the file k8s/secrets.yaml. The value of the "apikey" variable should be base64 encoded.